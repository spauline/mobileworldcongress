import scrapy
from random import choice
from string import ascii_uppercase

from mwc import items


class MWCSpider(scrapy.Spider):
    name = 'mwc'
    start_urls = [
            u'http://www.mobileworldcongress.com/exhibition/our-exhibitors/?exhibitor_keyword&exhibitor_location=991109', ]
    allowed_domains = ['mobileworldcongress.com']

    def __init__(self, file=None, *args, **kwargs):
        super(MWCSpider, self).__init__(*args, **kwargs)
        self.file_name = file if file is not None else (''.join(choice(ascii_uppercase) for _ in xrange(12)))

    @staticmethod
    def get_value(search_result):
        return None if len(search_result) == 0 else '\n'.join(search_result)

    def parse(self, response):
        next_page = response.xpath('//a[@class="next page-numbers"]/@href').extract()
        if len(next_page) != 0:
            next_page_url = response.xpath('//a[@class="next page-numbers"]/@href').extract()[0]
            exhibitors = response.xpath('//a[@class="exhibitor-box"]/@href').extract()
            for exhibitor_url in exhibitors:
                yield scrapy.Request(exhibitor_url, callback=self.parse_item)

            yield scrapy.Request(next_page_url, callback=self.parse)

    def parse_item(self, response):
        item = items.MwcItem()
        item['name'] = self.get_value(response.xpath('//div[@class="wrap-right clearfix"]/h2/text()').extract())
        item['location'] = self.get_value(response.xpath('//span[@class="ex-location"]/text()').extract())
        item['company_site'] = self.get_value(response.xpath('//a[.="Company website"]/@href').extract())
        item['description'] = self.get_value(response.xpath('//div[@class="wrap-content clearfix"]/descendant::p/text()').extract())
        yield item
