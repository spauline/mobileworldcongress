# -*- coding: utf-8 -*-


import scrapy


class MwcItem(scrapy.Item):
    name = scrapy.Field()
    location = scrapy.Field()
    company_site = scrapy.Field()
    description = scrapy.Field()

