import csv


class MwcPipeline(object):
    def process_item(self, item, spider):
        file_name = spider.file_name + '.csv'
        name = item['name'].encode('utf-8')
        location = item['location'].encode('utf-8')
        company_site = None if item['company_site'] is None else item['company_site'].encode('utf-8')
        description = None if item['description'] is None else item['description'].encode('utf-8')
        with open(file_name, 'ab') as csvfile:
            csvwriter = csv.writer(csvfile, delimiter=';', quoting=csv.QUOTE_MINIMAL)
            csvwriter.writerow([name, location, company_site, description])
        return item
